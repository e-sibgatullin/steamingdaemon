<?php

define('keyQueue', rand());

if($pidRunn = isRunning()){
    exit('Daemon is running pid '.$pidRunn.PHP_EOL);
}

$child_pid = pcntl_fork();
if ($child_pid) {
    exit();
}

regObj('Modbus\Interfaces\ParserInterface', 'Modbus\modbusParser');        
$plcSeting  = getPLCSeting();
$monitor    = getObj('ConnectionMonitor');
$feedBack   = getObj('FeedBack');
$daemon     = getObj('Daemon');

$daemon->setTask('ConnectionMonitor', $monitor, true);
$daemon->setTask('FeeedBack', $feedBack, true);

if (is_array($plcSeting)) {
    foreach ($plcSeting as $namePLC => $plc) {
        $PLC = getObj('PLC', $plc);
        $daemon->setTask($namePLC, $PLC);
    }
}

return $daemon;