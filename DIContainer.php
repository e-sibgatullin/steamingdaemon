<?php

class DIContainer 
{
    private static $objDi;    
    protected static $instance = []; 
    
    protected function __construct() { }
    
    protected function __clone() { }
    
    public function __wakeup()
    {
        throw new ConnectExceptions("Cannot unserialize a singleton.");
    }
    
    public static function getContainer()
    {
        if(empty(self::$objDi)){
            self::$objDi = new self;
        }
        
        return self::$objDi;        
    } 
    
    public function set($abstract, $concrete = null) 
    {
        if(is_null($concrete)){
            $concrete = $abstract;
        }
        
        static::$instance[$abstract] = $concrete;               
    }  

    public function get($abstract, $arguments = []) 
    {       
        if(!$this->has($abstract)){
            $this->set($abstract);
        }        
        
        return $this->resolve(static::$instance[$abstract], $arguments);
    }    
    
    public function has($abstract) 
    {
        if(array_key_exists($abstract, static::$instance)){
            return true;
        }
        
        return false;
    }
    
    private function resolve($concrete, $arguments) 
    {
        if($concrete instanceof Closure){             
            return $concrete($arguments);
        }        
           
        $reflector = new ReflectionClass($concrete);         
        $construct = $reflector->getConstructor();
       
        if(!$construct){
            return $reflector->newInstance();
        }
        
        $params = $construct->getParameters();        
        $dependencies = $this->getDependencies($params);
        $dependencies[] = $arguments;       
        return $reflector->newInstanceArgs($dependencies);
    }
    
    private function getDependencies($arguments) 
    {        
        $dependencies = [];
        
        foreach ($arguments as $argument) {            
            $dependency = $argument->getClass();
            if($dependency){
                $dependencies[] = $this->get($dependency->name);
            }
        }    
        
        return $dependencies;
    }
}