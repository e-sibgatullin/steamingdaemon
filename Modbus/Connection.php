<?php
namespace Modbus;

use Modbus\Interfaces\Connections;
use Modbus\Exceptions\ConnectExceptions;

class Connection implements Connections
{
    protected static $host;
    protected static $port;
    protected static $socket;
    
    static function getConnect($host = '127.0.0.1', $port = '502') 
    {        
        static::$host = $host;
        static::$port = $port;
        static::connect();
        return self::$socket;
    }    
    
    private static function connect() 
    {
        self::$socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);         
        socket_set_option(
                static::$socket, 
                SOL_SOCKET, 
                SO_RCVTIMEO, 
                array('sec' => 5, 'usec' => 0)
                );
        socket_set_option(
                static::$socket, 
                SOL_SOCKET, 
                SO_SNDTIMEO, 
                array('sec' => 5, 'usec' => 0)
                );
        if (FALSE === @socket_connect(self::$socket, self::$host, self::$port)) {           
            throw new ConnectExceptions(static::$host.' '.socket_strerror(socket_last_error()));           
        }        
    }   
    
    static function close($resource) 
    {
        if(!is_resource($resource)){
            return;
        }
        
        socket_set_option(
                $resource,
                SOL_SOCKET,
                SO_LINGER,
                ['l_onoff'=>1,'l_linger'=>0]
                );
        @socket_close($resource);
    }    
}
