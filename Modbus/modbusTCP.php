<?php
namespace Modbus;

use Modbus\Exceptions\ModbusExceptions;
use Modbus\Exceptions\ConnectExceptions;
use Modbus\Interfaces\ModbusInterface;
use Modbus\Interfaces\ParserInterface as Parser;

class modbusTCP implements ModbusInterface
{
    const FC1 = 1;              //Read Coil Status
    const FC2 = 2;              //Read Discrete Inputs
    const FC3 = 3;              //Read Holding Registers
    const FC4 = 4;              //Read Input Registers
    const FC5 = 5;              //Write Single Coil
    const FC6 = 6;              //Write Single Register   
    const FC15 = 15;            //Write multi Coil
    const FC16 = 16;            //Write multi Register
    const COILON = 'FF00';       //Coil Status ON 
    const COILOFF = '0000';      //Coil Status OFF
    const WRITE = 'W';          //Write Register
    const READ = 'R';           // Read Register
    
    protected $setings;
    protected $parser;
    protected $connection;   
    
    private $packet;
    private $responsePacket;
    private $answer;    

    public function __construct(Parser $parser, array $setings) 
    {
        $this->setings = $setings;      
        $this->connection = modbusConnect($this->setings['connection']['host'], $this->setings['connection']['port']);        
        $this->parser = $parser;        
    }   
    
    private function send() 
    {        
        $writeResult = @socket_write($this->connection, $this->packet);          
        if($writeResult == 0 or $writeResult === false){            
            throw new ConnectExceptions(socket_strerror(socket_last_error()));
        } 
        
        $this->responsePacket = @socket_read($this->connection, 2000);         
        if(false === $this->responsePacket or empty($this->responsePacket)) {             
            throw new ConnectExceptions(socket_strerror(socket_last_error()));
        }         
    }   
    
    private function fillData() 
    {
        foreach ($this->setings['plcPort'] as $regType => $ports) {
            $type = [];
            $totalReg = 0;            
            foreach ($ports as $port => $portParam) {
                list($aliasPort, $typePort) = $portParam;
                $type[$port] = $typePort;
                $totalReg += $typePort == 'float' ? 2 : 1;
            }
            $countReg = count($ports) > 1 ? max(key(end($ports))+1, $totalReg) : 1;
            $this->buildPacket(
                    $this->getFC($regType, self::READ), 
                    key(reset($ports)), 
                    $countReg
                    );
            
            $this->send();
            $this->answer[$regType] = $this->parser->run($this->responsePacket, $type);            
        }       
    }
    
    private function buildPacket($fc = self::FC3, $startReg = 0, $countReg = 1, $addrDev = 1) 
    {       
        $msgLenght = 6;
        if($fc == self::FC5 and $countReg >= 1){
            $countReg = self::COILON;
        }elseif($fc == self::FC5 and $countReg == 0){
            $countReg = self::COILOFF;
        }else{
            $countReg = $this->completeHex($countReg, 4);                       
        }
        
        if($fc == self::FC16){
            $msgLenght = 11;
            $countRegWrite = $this->completeHex(strlen($countReg)/4, 4);
            $countByte = $this->completeHex(strlen($countReg)/2,2);            
            $countReg = $countRegWrite.$countByte.$countReg;
        }
                        
        $this->packet = hex2bin( 
                $this->completeHex(rand(0, 65000), 4).
                $this->completeHex(0, 4).
                $this->completeHex($msgLenght, 4).
                $this->completeHex($addrDev, 2).
                $this->completeHex($fc, 2).
                $this->completeHex($startReg, 4).
                $countReg
                );
    }   
    
    private function completeHex($data, $length) 
    {        
        if(gettype($data) == 'double'){
            $hex = bin2hex(pack('f', $data));
            $hexByte = str_split($hex,2);
            return $hexByte[1].$hexByte[0].$hexByte[3].$hexByte[2];
        }
               
        return str_pad(dechex($data), $length, '0', STR_PAD_LEFT);
    }   
    
    private function getValueByAlias($alias) 
    {       
        $port = $this->getPortByAlias($alias);  
        $fc = $this->getFC($port['regType'], self::READ);
        $countReg = $port['portType'] == 'float' ? 2 : 1;
        $this->buildPacket($fc, $port['port'], $countReg);               
        $this->send();
        $value = $this->parser->run(
                $this->responsePacket, 
                [$port['portType']]
                );    
        return array_shift($value);
    }
    
    private function setValueByAlias($alias, $value) 
    {       
        $port = $this->getPortByAlias($alias);
        $fc = $port['portType'] == 'float' ? self::FC16 : $this->getFC($port['regType'], self::WRITE);
        $this->buildPacket($fc, $port['port'], $value);
        $this->send();        
    }
    
    private function getPortByAlias($portAlias) 
    {
        $found = [];
        foreach ($this->setings['plcPort'] as $typeReg => $ports) {
            foreach ($ports as $port => $portParam) {
                list($alias, $type) = $portParam;
                if($portAlias == $alias){
                    $found['port'] = $port;
                    $found['alias'] = $alias;
                    $found['portType'] = $type;
                    $found['regType'] = $typeReg;
                }
            }
        }
        
        if(empty($found)){
            throw new ModbusExceptions("Порт с именем $portAlias отсутсвует в списке портов!");
        }
        
        return $found;
    }
    
    private function getFC($typeReg = 'H', $typeComand = self::READ) 
    {
        if ($typeReg == 'H' and $typeComand == self::READ) {
            $fc = self::FC3;
        } elseif ($typeReg == 'D' and $typeComand == self::READ) {
            $fc = self::FC2;
        } elseif ($typeReg == 'C' and $typeComand == self::READ) {
            $fc = self::FC1;
        } elseif ($typeReg == 'I' and $typeComand == self::READ) {
            $fc = self::FC4;
        } elseif ($typeReg == 'H' and $typeComand == self::WRITE){
            $fc = self::FC6;
        } elseif ($typeReg == 'C' and $typeComand == self::WRITE){
            $fc = self::FC5;
        } elseif ($typeReg == 'D' or $typeReg == 'I' and $typeComand == self::WRITE){
            throw new ModbusExceptions('В порт данного типа запись не возможна!');            
        }        
        return $fc;
    }
    
    public function getAll() 
    {        
        $this->fillData();
        return $this->answer;
    }
    
    public function __get($aliasPort) 
    {               
        return $this->getValueByAlias($aliasPort);       
    }
    
    public function __set($aliasPort, $value) 
    {
        $this->setValueByAlias($aliasPort, $value);
    }
    
    public function __destruct() 
    {
        Connection::close($this->connection);
    }
}
