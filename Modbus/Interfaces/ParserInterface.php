<?php
namespace Modbus\Interfaces;

interface ParserInterface 
{
    public function run($modbusResponse, array $portMap);
}
