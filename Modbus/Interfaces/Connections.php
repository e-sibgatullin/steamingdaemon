<?php
namespace Modbus\Interfaces;

interface Connections
{    
    static function getConnect($host, $port);
    static function close($resource);
}
