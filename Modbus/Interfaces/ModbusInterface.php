<?php
namespace Modbus\Interfaces;
use Modbus\Interfaces\ParserInterface as Parser;

interface ModbusInterface 
{
    public function __construct(Parser $parser, array $setings);
    public function __set($portAlias, $portValue);
    public function __get($portAlias);
    public function getAll();
    
}
