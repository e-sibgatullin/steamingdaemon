<?php
namespace Modbus;

use Modbus\Exceptions\ParserExceptions;
use Modbus\Interfaces\ParserInterface;

class modbusParser implements ParserInterface
{
    private $modbusResponse;
    private $answer;
    private $portType;

    public function run($modbudResponse, array $portType) 
    {
        $this->modbusResponse = $modbudResponse;
        $this->portType = $portType;
        $this->checkError();
        $this->answer = [];
        if(ord($this->modbusResponse[7]) == 1 or ord($this->modbusResponse[7]) == 2){             
            $this->parsDIO();            
        }        
       
        if(ord($this->modbusResponse[7]) == 3 or ord($this->modbusResponse[7]) == 4){            
            $this->parsAIO();            
        } 
          
        return $this->answer;
    }
    
    private function checkError() 
    {
        if ((ord($this->modbusResponse[7]) & 0x80) > 0) {
            $failure_code = ord($this->modbusResponse[8]);
            $failures = array(
                0x01 => "ILLEGAL FUNCTION",
                0x02 => "ILLEGAL DATA ADDRESS",
                0x03 => "ILLEGAL DATA VALUE",
                0x04 => "SLAVE DEVICE FAILURE",
                0x05 => "ACKNOWLEDGE",
                0x06 => "SLAVE DEVICE BUSY",
                0x08 => "MEMORY PARITY ERROR",
                0x0A => "GATEWAY PATH UNAVAILABLE",
                0x0B => "GATEWAY TARGET DEVICE FAILED TO RESPOND");
            // get failure string
            if (key_exists($failure_code, $failures)) {
                $failure_str = $failures[$failure_code];
            } else {
                $failure_str = "UNDEFINED FAILURE CODE";
            }            
            throw new ParserExceptions($failure_code);            
        }
    }
    
    private function parsAIO() 
    {
        $data = str_split(bin2hex(substr($this->modbusResponse, 9)), 4);
        
        foreach ($this->portType as $key => $type) {    
            if($type == 'float'){                
                $this->answer[$key] = $this->hexToFloat($data[$key+1].$data[$key]);
            } else {
                $this->answer[$key] = hexdec($data[$key]);
            }            
        }        
    }
    
    private function parsDIO() 
    {       
        $countByte = bin2hex(substr($this->modbusResponse, 8,1));
        $data = bin2hex(substr($this->modbusResponse, 9, hexdec($countByte)));        
        $regData = '';
        foreach (str_split($data,2) as $value) {
            $byte = base_convert($value, 16, 2);
            $completeByte = str_pad($byte, 8, "0", STR_PAD_LEFT);
            $regData .= strrev($completeByte);            
        }     
        
        if(array_key_first($this->portType)>0){
            $completeData = str_pad('', array_key_first($this->portType), "0", STR_PAD_LEFT);
            $regData = $completeData.$regData;
        }
        
        foreach ($this->portType as $key => $type) {              
            $this->answer[$key] = $regData[$key];                      
        }        
    }
    
    private function hexToFloat($value) 
    {
        $bitData = base_convert($value, 16, 2);
        $bitComplete = str_pad($bitData, 32, "0", STR_PAD_LEFT);        
        $sign = pow(-1, (base_convert(substr($bitComplete, 0 ,1), 2, 10))); 
        $exponent = pow(2,(base_convert(substr($bitComplete, 1 ,8), 2, 10)-127)); 
        $mantissa = base_convert(substr($bitComplete, 9), 2, 10)/pow(2, 23)+1;
        
        return round($sign*$exponent*$mantissa, 2);
    }
}


