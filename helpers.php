<?php

if (!function_exists('getParam')) {
    function getParam($key, $default = null) {
        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'config')) {
            
            $file = new SplFileObject(__DIR__ . DIRECTORY_SEPARATOR . 'config');
            $file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
            $param = [];
            foreach($file as $line) {                 
                $line = trim($line);                
                list ($keys, $value) = explode('=',$line); 
                $param[$keys] = $value;
            }            
        }
        
        if(array_key_exists($key, $param)){
            return $param[$key];
        }
            
        return $default;
    }

}

if(!function_exists('getPLCSeting')){
    function getPLCSeting($namePLC = '') {
        $setings = include 'PLCConfig.php';
        if(empty($namePLC)){
            return $setings;
        }
        
        return $setings[$namePLC];        
    }
}

if(!function_exists('dump')){
    function dump($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}

if(!function_exists('getContainer')){
    function getContainer() {                          
        return DIContainer::getContainer();     
    }
}

if(!function_exists('getObj')){
    function getObj($nameObj, $arg = []) {
        return getContainer()->get($nameObj,$arg); 
    }
}

if(!function_exists('regObj')){
    function regObj($abstract, $concrete) {
        getContainer()->set($abstract, $concrete);     
    }
}

if(!function_exists('modbusConnect')){
    function modbusConnect($host, $port) {
        return Modbus\Connection::getConnect($host, $port);    
    }
}

if(!function_exists('errorHandler')){
    function errorHandler($errNo, $errStr, $errFile, $errLine) {             
        foreach (get_defined_constants(true)['Core'] as $strName => $numb) {
            if(preg_match('/^E_/', $strName)){
                $erroCode[$numb] = $strName; 
            }
        }               
        
        if(preg_match('/stream_(select|socket_accept)|socket_connect/', $errStr)){
            return;
        }
        
        error_log($errStr.' '.$errFile.' '.$errLine);     
        //error_log($errStr);     
    }
}

if(!function_exists('isRunning')){
    function isRunning() {
        $pidFile = __DIR__. DIRECTORY_SEPARATOR . getParam('PID_FILE_NAME');
        if(file_exists($pidFile) and filesize($pidFile)>0){
            $pidRunning = fopen($pidFile, 'r');
            $pid = fgets($pidRunning);
            fclose($pidRunning);
            return $pid;
        }
        
        return false;        
    }
}