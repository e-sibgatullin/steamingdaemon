<?php
use Modbus\modbusTCP;
use Modbus\Interfaces\ParserInterface as Parser;

class PLC extends modbusTCP
{   
    const STEAM_ON = 64;
    const STEAM_OFF = 128;
    const PLCOffline = 0;    
    const PLCOnline = 1;
    const PLCReconn = 2;    
    const MAX_SIZE_MSG = 200;
    
    protected $state = self::PLCOffline;
    protected $attempts = 0;
    protected $timeRec;
    protected $tableDB;
    protected $db;    
    private $queueMsg;
    private $shm;

    public function __construct(Parser $parser, array $plcSetings) 
    {        
        $this->db = getObj('DataBase\DBDML');
        $this->tableDB = $plcSetings['tableDB'];
        $this->queueMsg = msg_get_queue(keyQueue);
        try {
            parent::__construct($parser, $plcSetings);
            $this->state = true;
        } catch (Modbus\Exceptions\ConnectExceptions $e) {
            $this->setings = $plcSetings;
            $this->parser = $parser;
        }      
        
        $shmId = substr($plcSetings['connection']['host'], strripos($plcSetings['connection']['host'], '.')+1);        
        $this->shm = shmop_open($shmId, 'c', 0666, self::MAX_SIZE_MSG);
    }    
    
    public function run() 
    {     
        $this->ShMHandling();        
        if($this->state == self::PLCOnline and $this->steamSwitch){            
           $this->insertDB();
        }        
    }    
    
    public function getAll() 
    {
        if(!$this->state){
            return ['error' => 'offline'];
        }
        
        try {
            $data = parent::getAll();
        } catch (Modbus\Exceptions\ConnectExceptions $e) {
            $this->state = self::PLCOffline;
            return ['error' => 'offline'];
        } catch (Modbus\Exceptions\ParserExceptions $e) {            
            return ['error' => $e->getMessage()];
        } catch (Modbus\Exceptions\ModbusExceptions $e) {            
             return ['error' => $e->getMessage()];
        }       
        
        return [
            'steamT1' => $data['H'][0], 
            'steamT2' => $data['H'][1],
            'steamNumb' => $data['H'][2],
            'steamSwitch' => $data['H'][3],
            'steamValve' => $data['H'][4]
                ];
    }
    
    private function insertDB()
    {               
        if(!$this->isTime()){
            return;
        }        
        
        $data = [
            'ts' => date('Y-m-d H:m:s'),
            't1' => $this->t1,
            't2' => $this->t2,
            'steamNumb' => $this->steamNumb,
            'steamValve' => $this->steamValve
        ];
        echo $this->setings['connection']['host'].' : '.json_encode($data).PHP_EOL;
        //$this->db->insert($this->tableDB, $data);        
    }  
    
    private function isTime() 
    {
        if(empty($this->timeRec)){
            $this->timeRec = time()+getParam('TIMEOUT_REC');
            return false;
        }
        
        if($this->timeRec < time()){
            $this->timeRec = time()+getParam('TIMEOUT_REC');
            return true;
        }
        
        return false;
    }    
        
    private function attemptConnect() 
    {
        try {
            $this->connection = modbusConnect(
                    $this->setings['connection']['host'],
                    $this->setings['connection']['port']
            );                  
            $this->state = self::PLCOnline;            
        } catch (Modbus\Exceptions\ConnectExceptions $e) {
            trigger_error("Попытка подключения к {$this->setings['connection']['host']} не удалась. $e");
            $this->state = self::PLCOffline;
        }           
        
        return $this->state;
    }
    
    private function ShMHandling() 
    {        
        $host = $this->setings['connection']['host'];        
        $readShm = trim(shmop_read($this->shm, 0, self::MAX_SIZE_MSG));        
        $feedBackData = unserialize($readShm);        
        if(is_array($feedBackData) and $feedBackData[$host]['w'] <> false){
            $operation = $feedBackData[$host]['w'];            
            $this->$operation();
        }
        
        if(is_array($feedBackData) and $feedBackData[$host]['state'] == self::PLCReconn){
            $this->attemptConnect();                        
        }
        
        $answerPLC = $this->state ? $this->getAll() : ['error' => 'offline'];
        $data = [$host => ['r' => $answerPLC, 'w' => false, 'state' => $this->state]];
        shmop_write($this->shm, serialize($data), 0);
    }
    
    public function steamOn() 
    {
        $this->steamSwitch = self::STEAM_ON;
        return true;
    }
    
    public function steamOff() 
    {
        $this->steamSwitch = self::STEAM_OFF;
        return true;
    }
    
    public function __get($port) 
    {
        if(!$this->state){
            return false;
        }
        
        try {
            return parent::__get($port);
        } catch (Modbus\Exceptions\ConnectExceptions $e) {
            $this->state = self::PLCOffline;
            return false;
        } catch (Modbus\Exceptions\ParserExceptions $e) {            
            return $e->getMessage();
        } catch (Modbus\Exceptions\ModbusExceptions $e) {            
            return $e->getMessage();
        } 
    }
    
    public function __set($port, $value) 
    {
        if(!$this->state){
            return false;
        }
        
        try {
            parent::__set($port, $value);
        } catch (Modbus\Exceptions\ConnectExceptions $e) {
            $this->state = self::PLCOffline;
            return 'offline';
        } catch (Modbus\Exceptions\ParserExceptions $e) {            
            return $e->getMessage();
        } catch (Modbus\Exceptions\ModbusExceptions $e) {            
            return $e->getMessage();
        } 
    }
    
    public function __destruct() {
        shmop_delete($this->shm);
        shmop_close($this->shm);
        parent::__destruct();
    }
}
