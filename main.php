#!/usr/bin/php
<?php
ini_set('error_log',__DIR__.'/logs/error.log');
date_default_timezone_set('Europe/Moscow');

require_once __DIR__.'/autoload.php';
require_once __DIR__.'/helpers.php';
$daemon = require_once __DIR__.'/startInit.php';

posix_setsid();
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);
set_error_handler('errorHandler');

$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen(__DIR__.'/logs/application.log', 'ab');
$STDERR = fopen(__DIR__.'/logs/daemon.log', 'ab');

$daemon->run();