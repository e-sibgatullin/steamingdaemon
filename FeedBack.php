<?php

class FeedBack
{
    const timedOut = 5;    
    const MAX_SIZE_MSG = 200;
    const ACCESS_RIGHTS = 0666;

    private $socket;
    private $connections = [];
    private $timedOut = [];
    private $read = [];
    private $write = null;
    private $except = null;    
    private $fedBackData; 
    private $shm = [];

    public function __construct() 
    {        
        foreach (getPLCSeting() as $plcName => $plcParam) {
            $id = substr($plcParam['connection']['host'], strripos($plcParam['connection']['host'], '.')+1);            
            $this->shm[$plcName] = shmop_open($id, 'c', self::ACCESS_RIGHTS, self::MAX_SIZE_MSG);            
        }
        $this->createSocket();        
    }        
    
    private function createSocket() 
    {
        $this->socket = stream_socket_server("tcp://10.0.3.200:8000", $errno, $errstr);
        if (!$this->socket) {            
            return false;
        }                 
    }
    
    public function run()
    {        
        $this->acceptConnect();
        $this->handlingState();
        $this->handlingTimeOut();                     
    }    
    
    private function acceptConnect() 
    {
        if ($connect = stream_socket_accept($this->socket, empty($this->connections) ? -1 : 0, $peer)) {            
            $this->connections[$peer] = $connect;            
            $this->timedOut[$peer] = time() + self::timedOut;
        }
    }
    
    private function handlingState() 
    {
        $this->read = $this->connections;
        if (stream_select($this->read, $this->write, $this->except, 0) > 0) {
            foreach ($this->read as $name => $conn) {
                $data = trim(stream_get_line($conn, 100, PHP_EOL));                 
                if(empty($data)){
                    $this->closeConnect($name);
                    continue;
                }
                
                $this->handlingReq($data);                  
                fwrite($conn, json_encode($this->fedBackData));                
                $this->timedOut[$name] = time() + self::timedOut;
            }
        }
    }
    
    private function handlingTimeOut() 
    {
        foreach ($this->timedOut as $nameClient => $time) {
            if ($time < time()) {
                $this->closeConnect($nameClient);
            }
        }
    }
    
    private function handlingReq($contents) 
    {        
        $this->fedBackData = [];        
        foreach ($this->shm as $plcName => $shmResource) {                                    
            $shmData = $this->shmRW($plcName);
            $PLCData[$plcName] = $shmData[key($shmData)]['r'];                        
        }        
        
        if(stripos($contents, '=')){
            $this->steamSwitch($contents, $PLCData);            
            return;            
        }
        
        if($contents == 'getAll'){
            $this->fedBackData = $PLCData;  
            return;
        }
        
        if(array_key_exists($contents, $PLCData)){
            $this->fedBackData[$contents] = $PLCData[$contents];
            return;
        }        
        
        if(empty($this->fedBackData)){
            $this->fedBackData['error'] = 'не подерживаемая команда.';            
        }        
    }
    
    private function steamSwitch($comand, $PLCData) 
    {        
        list($plcName, $steamOnOff) = explode('=', $comand);        
        if(array_key_exists($plcName, $PLCData)){                                                 
            $data = $this->shmRW($plcName);
            $data[key($data)]['w'] = $steamOnOff;             
            $this->shmRW($plcName, $data);
            $this->fedBackData[$plcName] = $steamOnOff;            
            return;
        }        
                    
        $this->fedBackData['error'] = "Устроиство $plcName не найдено.";
    }    
    
    private function closeConnect($connName) 
    {
        fclose($this->connections[$connName]);
        unset($this->connections[$connName]);
        unset($this->timedOut[$connName]);         
    }
    
    private function shmRW($shmId, $writeData = null) 
    {
        if($writeData == null){
            $read = trim(shmop_read($this->shm[$shmId], 0, self::MAX_SIZE_MSG));            
            $data = unserialize($read);            
            return $data;
        }
        
        shmop_write($this->shm[$shmId], serialize($writeData), 0);
    }
    
    public function closeSocket() 
    {
        if($this->socket === FALSE){
            return;
        }
        
        fclose($this->socket);
    }    
}
