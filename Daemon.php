<?php

class Daemon 
{    
    private $stop_server = false;
    private $currentJobs = array();
    private $tasks;
    
    public $maxProcesses = 5;    

    public function __construct() 
    {        
        set_error_handler("errorHandler");
        pcntl_signal(SIGTERM, array($this, "childSignalHandler"));
        pcntl_signal(SIGCHLD, array($this, "childSignalHandler"));         
        $this->createPidFile();         
    }    
    
    public function childSignalHandler($signo, $pid = null, $status = null) 
    {
        switch ($signo) {
            case SIGTERM:                
                $this->stopDaemon();
                break;
            case SIGCHLD:                 
                $this->childProcess($pid, $status);
                break;
            default:            
        }
    }
    
    public function setTask($alias, $task, $newProc = false) 
    {
        $this->tasks[$alias] = ['task' => $task, 'createNewProc'=>$newProc];
    }
    
    public function run() 
    {        
        while (!$this->stop_server) {            
            while (count($this->currentJobs) >= $this->maxProcesses) {
                trigger_error("Maximum children allowed, waiting...");
                sleep(1);
            }            
            
            foreach ($this->tasks as $key => $task) {
                if ($task['createNewProc']) {
                    $this->addJob($key, $task['task']);
                    continue;
                }
                
                $task['task']->run();
            }

            pcntl_signal_dispatch();
            usleep(100000);             
        }
    }

    protected function addJob($jobName, $job) 
    {        
        if(array_key_exists($jobName, $this->currentJobs)){
            return;
        }
        
        $pid = pcntl_fork();
        if ($pid == -1) {            
            trigger_error('Не удалось создать дочерний процесс');
            return FALSE;
        } elseif ($pid) {               
            $this->currentJobs[$jobName][$pid] = true;
        } else {            
            while (true){
                $job->run();
                usleep(100000);
            }            
            exit;
        }                
    }
        
    private function searchPidJob($pid)
    {
        if(empty($this->currentJobs)){
            return;
        }
        
        foreach ($this->currentJobs as $key => $value) {
            if (array_key_exists($pid, $value)) {
                unset($this->currentJobs[$key]);
            }
        }
    } 
    
    private function createPidFile() 
    {
        $pidFile = fopen(__DIR__. DIRECTORY_SEPARATOR . getParam('PID_FILE_NAME'), 'ab');
        fwrite($pidFile, getmypid());
        fclose($pidFile);
    }
    
    private function stopDaemon() 
    {
        if (!empty($this->currentJobs)) {
            foreach ($this->currentJobs as $value) {
                posix_kill(key($value), SIGHUP);
            }
        }
        
        $this->stop_server = true;
        unlink(__DIR__. DIRECTORY_SEPARATOR . getParam('PID_FILE_NAME'));        
    }
    
    private function childProcess($pid, $status) 
    {
        if (!$pid) {
            $pid = pcntl_waitpid(-1, $status, WNOHANG);
        }
        
        while ($pid > 0) {
            $this->searchPidJob($pid);
            $pid = pcntl_waitpid(-1, $status, WNOHANG);
        }
    }     
}
