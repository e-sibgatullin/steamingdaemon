<?php

namespace DataBase;

use DataBase\DBConnect;

class DBDML 
{    
    const REPLACE = 'REPLACE';
    const IGNORE = 'IGNORE';
    
    private $dbconnect;
    
    public $fetchAll = true;
    public $recMode = self::IGNORE;
    private $connectName = 'default';


    public function __construct() 
    {        
        $this->dbconnect = DBConnect::getConnect($this->connectName);                
    }
    
    public function on($connName) 
    {        
        $this->dbconnect = DBConnect::getConnect($connName);  
        return $this;
    }
    
    public function select($sql, array $params = []) 
    {        
        $query = $this->dbconnect->prepare($sql);           
        $query->execute($params);                 
        return $this->fetchAll ? $query->fetchAll():$query->fetch(); 
    }
    
    public function insert($table, array $params) 
    {
        $field = sprintf("(%s)",implode(',', array_keys($params)));
        $mask = sprintf('(:%s)',implode(', :', array_keys($params)));
        $sql = sprintf('INSERT INTO %s %s VALUES %s', $table,$field,$mask);        
        $query = $this->dbconnect->prepare($sql);        
        $query->execute($params);        
    }
    
    public function loadData($table, $file, array $field) 
    {
        $field = implode(", ", $field);
        $load = sprintf("LOAD DATA INFILE '%s' %s INTO TABLE %s FIELDS TERMINATED BY ';' (%s)",$file,$this->recMode,$table,$field);        
        $this->dbconnect->exec($load);        
    }
}
