<?php
namespace DataBase;

class DBConnect
{       
    private static $instance;   
    private static $host;


    public static function getConnect($name)
    {        
        self::$host = getParam('DB_IP','10.0.3.200');
        if(self::$instance === null){
            self::$instance = self::PDOCon();
        }      
        
        return self::$instance;
    }    
    
    private static function PDOCon()
    {   
        $opt  = array(
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            );
        $stringConnect = sprintf('%s:host=%s;dbname=%s','mysql',self::$host,getParam('DB_BASE','concrete6'));        
        $link = New \PDO($stringConnect, 'root', 'mysql', $opt);
        $link->exec('SET NAMES UTF8');
        
        return $link;
    }    
}
