<?php
spl_autoload_register(function ($class) 
{        
    $path = __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';    
    $path = str_replace("\\", DIRECTORY_SEPARATOR, $path);        
    if (file_exists($path)) {         
        require_once($path);
    }    
    
});