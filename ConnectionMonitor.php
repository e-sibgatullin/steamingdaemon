<?php
use Modbus\Connection;

class ConnectionMonitor 
{
    const MAX_SIZE_MSG = 200;
    const PLCOffline = 0;        
    const PLCReconn = 2; 
    const ACCESS_RIGHTS = 0666;
    
    private $connection = [];
    private $shm = [];    
    
    public function __construct() 
    {        
        $plcSetings = getPLCSeting();
        foreach ($plcSetings as $plcName => $PLC) {
            $shmId = substr($PLC['connection']['host'], strripos($PLC['connection']['host'], '.')+1);
            $this->shm[$PLC['connection']['host']] = shmop_open($shmId, 'c', self::ACCESS_RIGHTS, self::MAX_SIZE_MSG);   
        }        
    }
    
    public function run() 
    {        
        foreach ($this->shm as $shmId => $resource) {
            $readShm = trim(shmop_read($resource, 0, self::MAX_SIZE_MSG));
            $feedBackData = unserialize($readShm);        
            if(is_array($feedBackData)){
                $this->connection[key($feedBackData)] = $feedBackData[key($feedBackData)];
            }
        }
        
        $this->handlerConnections();        
    }
    
    private function handlerConnections() 
    {
        foreach ($this->connection as $host => $data) {            
            if($data['state'] == self::PLCOffline){
                $this->connectionAttempt($host);
            }            
        }
    }
    
    private function connectionAttempt($host) 
    {        
        try {            
            $connect = Connection::getConnect($host);            
            Connection::close($connect);
        } catch (Modbus\Exceptions\ConnectExceptions $e) {            
            return;            
        }       
        
        sleep(1);        
        $this->connection[$host]['state'] = self::PLCReconn;        
        shmop_write($this->shm[$host], serialize([$host => $this->connection[$host]]), 0);        
    }    
}
