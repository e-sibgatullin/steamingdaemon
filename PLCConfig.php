<?php

/* [typeReg => [portNumb => [aliasPort, typePort]]]
 * typeReg (C - Coils, D - Discrete Inputs, I = Input Registers, H - Holding Registers)
 */

return [
    'test' => [
        'plcPort' => [
            'H' => [
                0 => ['t1', 'word'],
                1 => ['t2', 'word'],
                2 => ['steamNumb', 'word'],
                3 => ['steamSwitch', 'word'],
                4 => ['steamValve', 'float']
            ]
        ],
        'connection' => [
            'host' => '10.0.3.63',
            'port' => '502'
        ],
        'tableDB' => 'test'
    ],
    'kaseta' => [
        'plcPort' => [
            'H' => [
                0 => ['t1', 'word'],
                1 => ['t2', 'word'],
                2 => ['steamNumb', 'word'],
                3 => ['steamSwitch', 'word'],
                4 => ['steamValve', 'float']
            ]
        ],
        'connection' => [
            'host' => '10.0.3.67',
            'port' => '502'
        ],
        'tableDB' => 'test'
    ],
    'pustotka' => [
        'plcPort' => [
            'H' => [
                0 => ['t1', 'word'],
                1 => ['t2', 'word'],
                2 => ['steamNumb', 'word'],
                3 => ['steamSwitch', 'word'],
                4 => ['steamValve', 'float']
            ]
        ],
        'connection' => [
            'host' => '10.0.3.68',
            'port' => '502'
        ],
        'tableDB' => 'test'
    ]
];
